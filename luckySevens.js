function rollDice(minimum, maximum) {
    return Math.floor(Math.random() * (1 + maximum - minimum)) + minimum;
}


function play(){
	
	var minimum = 1;
	var maximum = 6;
	var die1;
	var die2;
	var startingBet = document.getElementById("startingBet").value;
	var bet = startingBet;
	var max = bet;
	var plays = 0;
	var highCount = 1;

	while (bet > 0){
		die1 = rollDice(minimum, maximum);
		die2 = rollDice(minimum, maximum);
		plays = plays +1;
		if (die1 + die2 == 7){
			bet = bet +4;
		}
		else {
			bet = bet - 1
		}
	
		if (bet > max){
			max = bet;
			highCount = plays;
		}


	}
	
document.getElementById("displayStartingBet").innerHTML = startingBet;
document.getElementById("displayTotalRolls").innerHTML = plays;
document.getElementById("displayHighest").innerHTML = max;
document.getElementById("displayRollAtHigh").innerHTML = highCount;
	
document.getElementById("playButton").value ="Play Again";
document.getElementById("startingBet").value = "";
	
results.style.display = "block";
console.log(max);
console.log(highCount);
console.log(plays);
}

